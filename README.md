# OpenML dataset: audit-data

https://www.openml.org/d/42931

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Nishtha Hooda, CSED, TIET, Patiala

**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Audit+Data) - 2018

**Please cite**: [Hooda, Nishtha, Seema Bawa, and Prashant Singh Rana. 'Fraudulent Firm Classification: A Case Study of an External Audit.' Applied Artificial Intelligence 32.1 (2018): 48-64.]( https://doi.org/10.1080/08839514.2018.1451032)

The goal of the research is to help the auditors by building a classification model that can predict the fraudulent firm on the basis the present and historical risk factors. The information about the sectors and the counts of firms are listed respectively as Irrigation (114), Public Health (77), Buildings and Roads (82), Forest (70), Corporate (47), Animal Husbandry (95), Communication (1), Electrical (4), Land (5), Science and Technology (3), Tourism (1), Fisheries (41), Industries (37), Agriculture (200). The original dataset was separated into a trial and audit dataset. In this dataset these are concatenated into 1 dataset. Two features (trial and audit) have been added to indicate whether the data was originally from the trial or audit dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42931) of an [OpenML dataset](https://www.openml.org/d/42931). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42931/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42931/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42931/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

